# Dax

Docker Action eXecutor

## Commands

```
$ dax config [--global]
$ dax init
$ dax checkout [branch|tag|commit]
$ dax build [--tag name] [directory]
$ dax pull [tag]
$ dax push [tag]
$ dax deploy [tag]
$ dax run [command] [args]
$ dax up
$ dax down
$ dax exec rake db:migrate
$ dax exec rails console
$ dax verify
$ dax lint
$ dax test
```

## Global Configuration

```
# .daxrc.yml
config:
  prefix: myrepo.io

build:
  autopull: true
  tag: latest
```

## Project Configuration

```
# .dax.yml
config:
  prefix: myrepo.io
  name: myproject

build:
  autopull: true
  tag: latest

run:
  compose: docker-compose.yml
  wrapper: bundle exec

lint:
  lang:
    - ruby
    - python
    - js
    - css
  ignore:
    - spec

test:
  - name: rspec
    compose: docker-compose.rspec.yml
    command: bundle exec rake
  - name: cucumber
    compose: docker-compose.cucumber.yml
    command:
      - bundle exec cucumber features/marketing
      - bundle exec cucumber features/billing_engine
      - bundle exec cucumber features/reader
      - bundle exec cucumber features/cms
```
