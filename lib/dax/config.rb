require "yaml"

module Dax
  class Config
    GLOBAL_CONFIG_FILE = ".daxrc.yml".freeze
    PROJECT_CONFIG_FILE = ".dax.yml".freeze
    DEFAULT_CONFIG = {
      "build" => {
        "prefix" => "",
        "pull" => true
      },
      "test" => []
    }

    def initialize
      validate_config!
    end

    def build
      raw_config["build"]
    end

    def tests
      raw_config["test"]
    end

    private

    def validate_config!
      validate_build!
      validate_tests!
    end

    def validate_build!
      raise "build.prefix must be a string" unless build["prefix"].is_a? String
      raise "build.prefix must be valid docker registry" unless /^(\w*)(\.\w+)*$/ === build["prefix"]
      raise "build.pull must be boolean" unless [true, false].include? build["pull"]
    end

    def validate_tests!
      raise "test must be an array" unless tests.is_a? Array
      tests.each do |test|
        raise "test must be defined as hash" unless test.is_a? Hash
        name = test["name"]
        raise "test must be have name" unless name.is_a? String
        raise "test.#{name} must have command set" unless test["command"].is_a? String
      end
    end

    def raw_config
      @raw_config ||= DEFAULT_CONFIG.merge(global_config).merge(dynamic_config).merge(project_config)
    end

    def global_config
      return {} unless File.exists?(GLOBAL_CONFIG_FILE)
      @global_config ||= YAML.load(File.open(GLOBAL_CONFIG_FILE))
    end

    def dynamic_config
      {}
    end

    def project_config
      return {} unless File.exists?(PROJECT_CONFIG_FILE)
      @project_config ||= YAML.load(File.open(PROJECT_CONFIG_FILE))
    end
  end
end
