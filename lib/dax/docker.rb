module Dax
  class Docker
    def build(options={})
      opts = []
      opts << "--pull" if options[:pull]
      exec("docker", "build", *opts, options[:dir] || ".")
    end
  end
end
