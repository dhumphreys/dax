module Dax
  class CLI
    def initialize
      @config = Dax::Config.new
    end

    def config(global=false)
    end

    def init(dir=nil)
    end

    def checkout(target=nil)
    end

    def build(options={})
      options[:dir] ||= "."
      options[:name] ||= @config.build["name"]
      options[:prefix] ||= @config.build["prefix"]
      options[:pull] ||= @config.build["pull"]
      options[:tag] ||= @config.build["tag"]
      docker.build(options)
    end

    def pull(tag=nil)
    end

    def push(tag=nil)
    end

    def deploy(tag=nil)
    end

    def run(command)
    end

    def up(service_list=nil)
    end

    def down
    end

    def verify
    end

    def lint
    end

    def test
    end

    private

    def docker
      @docker ||= Dax::Docker.new
    end
  end
end
