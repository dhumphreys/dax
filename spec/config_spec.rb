require "spec_helper"

describe Dax::Config do
  context "default config" do
    it "passes validation" do
      expect { subject }.not_to raise_error
    end

    context "#build" do
      it "returns build options" do
        expect(subject.build).to eq("prefix" => "", "pull" => true)
      end
    end

    context "#tests" do
      it "returns empty list of tests" do
        expect(subject.tests).to eq([])
      end
    end
  end
end
