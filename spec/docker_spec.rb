require "spec_helper"

describe Dax::Docker do
  before { allow(subject).to receive(:exec) }

  describe "#build" do
    context "without args" do
      it "calls docker build --pull" do
        expect(subject).to receive(:exec).with(*%w(docker build --pull path/to/dir))
        subject.build("path/to/dir")
      end
    end

    context "with --pull" do
      it "calls docker build --pull" do
        expect(subject).to receive(:exec).with(*%w(docker build --pull path/to/dir))
        subject.build("path/to/dir", true)
      end
    end

    context "without --pull" do
      it "calls docker build" do
        expect(subject).to receive(:exec).with(*%w(docker build path/to/dir))
        subject.build("path/to/dir", false)
      end
    end
  end
end
