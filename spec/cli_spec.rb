require "spec_helper"

describe Dax::CLI do
  let(:docker) { Dax::Docker.new }
  before { allow(subject).to receive(:docker).and_return(docker) } 

  describe "#build" do
    context "without args" do
      it "calls Docker#build with --pull" do
        expect(docker).to receive(:build).once.with("path/to/dir", true)
        subject.build("path/to/dir")
      end
    end

    context "with --pull=true" do
      it "calls Docker#build with --pull" do
        expect(docker).to receive(:build).once.with("path/to/dir", true)
        subject.build("path/to/dir", true)
      end
    end

    context "with --pull=false" do
      it "calls Docker#build without --pull" do
        expect(docker).to receive(:build).once.with("path/to/dir", false)
        subject.build("path/to/dir", false)
      end
    end
  end
end
