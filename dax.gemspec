$LOAD_PATH.unshift File.expand_path("../lib", __FILE__)
require 'dax/version'

Gem::Specification.new do |s|
  s.name = "dax"
  s.summary = "dax!"
  s.version = Dax::VERSION
  s.description = "Command-line interface for performing docker operations"
  s.authors = ["Don Humphreys", "Sam Lachance"]
  s.email = ["dhumphreys88@gmail.com", "sam@samlachance.com"]
  s.license = "MIT"

  s.required_ruby_version = ">= 1.9.0"
  s.required_rubygems_version = ">= 1.3.5"

  s.add_dependency "thor", "~> 0.20.0"
  s.add_development_dependency "bundler", "~> 1.0"
  s.add_development_dependency "rspec", "~> 3.5"

  s.bindir = "bin"
  s.files = Dir["{bin,config,lib}/**/*"] + %w(LICENSE)
  s.executables = %w(dax)
end
